# Proof-of-concept
import cv2
import sys
from constants import *
from emotion_recognition import EmotionRecognition
import numpy as np
import json

cascade_classifier = cv2.CascadeClassifier(CASC_PATH)

def brighten(data, b):
    datab = data * b
    return datab

def format_image(image):
    try:
        if len(image.shape) > 2 and image.shape[2] == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            image = cv2.imdecode(image, cv2.CV_LOAD_IMAGE_GRAYSCALE)
        faces = cascade_classifier.detectMultiScale(
            image,
            scaleFactor=1.3,
            minNeighbors=5
        )
        # None is we don't found an image
        if not len(faces) > 0:
            return None
        max_area_face = faces[0]
        for face in faces:
            if face[2] * face[3] > max_area_face[2] * max_area_face[3]:
                max_area_face = face
        # Chop image to face
        face = max_area_face
        image = image[face[1]:(face[1] + face[2]), face[0]:(face[0] + face[3])]
        # Resize image to network size
        try:
            image = cv2.resize(image, (SIZE_FACE, SIZE_FACE),
                               interpolation=cv2.INTER_CUBIC) / 255.
        except Exception:
            print("[+] Problem during resize")
            return None
        return image
    except Exception:
         print ("Acabou o video")

def save_log(exp_log):
    with open("Output.txt", "w") as text_file:
        buffer = ""
        reverse_log = exp_log[::-1]
        for s in reverse_log:
            buffer = s + '\n' + buffer
        text_file.write(buffer)

def index2expression(i):
    exp_txt = ['Raiva', 'Nojo', 'Medo',
                'Alegria', 'Tristeza', 'Surpresa', 'Neutra']
    return exp_txt[i]

def video_mode(path = None):

    master_counter = -1
    #AVR_VALUE = 1
    frame_array = []
    micro_array = []
    frame_counter = micro_counter = counter_me = count = 0
    result_0 = result_1 = result_2 = result_3 = result_4 = result_5 = result_6 = 0.0
    f_result = None
    flag_me = False
    expression_msg = ""
    expression_log = []
    # Load Model
    network = EmotionRecognition()
    network.build_network()

    video_capture = cv2.VideoCapture(0)

    if path != None and path != "live":
      video_capture = cv2.VideoCapture(path)

    font = cv2.FONT_HERSHEY_SIMPLEX

    feelings_faces = []
    for index, emotion in enumerate(EMOTIONS):
        feelings_faces.append(cv2.imread('./emojis/' + emotion + '.png', -1))

    while True:
        # Capture frame-by-frame
        ret, frame = video_capture.read()
        frame_array.append(frame)
        count += 1


        if count == AVR_VALUE:
            master_counter += 1
            count = 0
            frame_counter += AVR_VALUE

            for frame in frame_array:
                result = network.predict(format_image(frame))
                if result is None:
                    save_log(expression_log)
                    return
                result_0 = result[0][0] + result_0
                result_1 = result[0][1] + result_1
                result_2 = result[0][2] + result_2
                result_3 = result[0][3] + result_3
                result_4 = result[0][4] + result_4
                result_5 = result[0][5] + result_5
                result_6 = result[0][6] + result_6

            frame_array = []
            result_0 = result_0/AVR_VALUE
            result_1 = result_1/AVR_VALUE
            result_2 = result_2/AVR_VALUE
            result_3 = result_3/AVR_VALUE
            result_4 = result_4/AVR_VALUE
            result_5 = result_5/AVR_VALUE
            result_6 = result_6/AVR_VALUE

            f_result = np.array([result_0, result_1,result_2,result_3,result_4,result_5,result_6])
            print (f_result)
            result_0 = result_1 = result_2 = result_3 = result_4 = result_5 = result_6 = 0.0

            #de 6 a 16 = micro
            micro_array.append(np.argmax(f_result))

            if len(micro_array) > 0:
                index = master_counter - 1
                expression_msg = index2expression(micro_array[master_counter])
                if micro_array[index] == micro_array[master_counter]:
                    micro_counter += 1
                else:
                    if micro_counter > 6 and micro_counter < 16:
                        flag_me = True
                        print ("Micro Expressão:", expression_msg, "- Frame:", frame_counter)
                        expression_log.append("Micro Expressão: " + str(expression_msg) + " - Frame: " + str(frame_counter))
                    micro_counter = 0
                print ("Expressão:", expression_msg, "- Frame:", frame_counter)
                expression_log.append("Expressão: " + str(expression_msg) + " - Frame: " + str(frame_counter))

        if f_result is not None:
            for index, emotion in enumerate(EMOTIONS):
                cv2.putText(frame, emotion, (10, index * 20 + 20),
                            cv2.FONT_HERSHEY_PLAIN, 0.5, (0, 255, 0), 1)
                cv2.rectangle(frame, (130, index * 20 + 10), (130 +
                                                              int(f_result[index] * 100), (index + 1) * 20 + 4), (255, 0, 0), -1)

            face_image = feelings_faces[np.argmax(f_result)]

            # Ugly transparent fix
            for c in range(0, 3):
                frame[200:320, 10:130, c] = face_image[:, :, c] * \
                    (face_image[:, :, 3] / 255.0) + frame[200:320,
                                                          10:130, c] * (1.0 - face_image[:, :, 3] / 255.0)

        if flag_me == True:
            counter_me += 1
            if counter_me < 10:
                cv2.putText(frame, "M" + str(np.argmax(f_result) + 1), (620, 160),cv2.FONT_HERSHEY_PLAIN, 10, (0, 0, 255), 8)
            else:
                flag_me = False
                counter_me = 0
        cv2.imshow('Video', frame)
        #cv2.imwrite('images/frame_' + str(frame_counter) + '.png' ,frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            save_log(expression_log)
            break

    video_capture.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("treta")
    if sys.argv[1] == 'live':
        video_mode()
    else:
        video_mode(sys.argv[1])

video_mode()
