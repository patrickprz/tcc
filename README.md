##Dataset
https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data). 

##Instalação de requerimentos
pip install -r requirements.txt

##Treinamento
python3 emotion_recognition.py train

##Previsão das expressões em video
python3 video.py <local do video>

##OBS: O artigo original dos pesquisadores que proporcionaram o modelo estão na pasta Other, lá também pode ser visualizado o arquivo readme do projeto realizado por eles.